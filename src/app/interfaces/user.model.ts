export interface AttributeValue {
    uuid: string,
    value: any
}
export interface Role {
    uuid: string,
    name: string,
    status: number,
    isActive: boolean
}

export interface Option {
    uuid: string,
    name: string,
    description: string,
    labelID: string,
    mandatory: boolean,
    selected: boolean
}

export class User {
    
    constructor(
        public uuid: string, 
        public username: string, 
        public email: string,
        public mobile: string, 
        public firstName: string, 
        public lastName: string,
        public password: string,
        public address: {city: "", street: "" },
        public fiscalCode: string, 
        public vatNumber: string,
        public avatar: string,
        public picture: string,
        public info: string,
        public activeRoles: string[] = [],
        public roles: Role[] = [],
        public options: Option[] = [],
        public attributeValues: AttributeValue[] = [],
        public attributeTypes: string[] = [],
        public _isTrustedEmail: boolean = false,
        public _isTrustedMobile: boolean = false,
        public hasStripeID: boolean = false,
        public extra: any = {
            residence: {
                address: {
                }
            }
        }) {
    }

    isTrustedEmail():boolean {
        return this._isTrustedEmail==true;
    }

    isTrustedMobile():boolean {
        return this._isTrustedMobile==true;
    }
}