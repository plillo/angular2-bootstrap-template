import { Component, OnInit } from '@angular/core';
const YAML = require('yaml')

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  title = 'template-app';
  public mytime: Date = new Date();
  sss = YAML.stringify([true, false, 'maybe', null]);

  constructor() { }

  ngOnInit() {
  }

}
