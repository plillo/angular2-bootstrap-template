import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { BackendService } from 'src/app/services/backend.service';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MarkdownService } from 'ngx-markdown';


/*
Per inserire contenuti HTML in assets/html:
  <app-content name="wip" origin="assets"></app-content>

Per inserire contenuti remoti:
  <app-content name="tech" owner="902e1fb1-2dfe-4686-a3f8-a1689d5f7b89" topic="/contents/personal" origin="remote"></app-content>
*/

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  @Input() origin: string;
  @Input() owner: string;
  @Input() topic: string;
  @Input() name: string;
  @Input() continueLink: boolean = true;

  isOwner: boolean = false;
  content: string = '';

  constructor(private router: Router, private http: HttpClient, private backendService: BackendService, private userService: UserService, private sanitizer: DomSanitizer) {
  }
   
  ngOnInit() {
    switch(this.origin) {
      case 'assets':

        this.http.get('./assets/html/'+this.name+'.html', { responseType: "text" })
        .subscribe(
          (text) => {
            text = text.replace('[c]',''+text.search('[c]'));

            this.content = text;
          },
          () => {
            this.content = '#no page#';
          }
        );

        break;
      case 'remote':
        let url = this.backendService.getAuthBackend() + '/core/1.0/users/' +this.owner + '/contents?type=text&topic='+this.topic+'&name='+this.name;
        this.http.get(url, {})
        .subscribe(
          data =>{
            let dataContent = data['content'];
            this.content = dataContent['body']?dataContent['body']:'';

            if(this.continueLink) {
              let continuePosition = this.content.search('<c>');
              if(continuePosition>-1) {
                this.content =  this.content.substring(0, continuePosition);
                this.content += '...[<a href=\"#/content-by-url?name='+this.name+'&origin=remote&owner='+this.owner+'\">continua</a>]';
              }
            }

            this.isOwner = this.userService.getUser()? this.userService.getUser().uuid == dataContent['owner']:false;
          }, 
          err=>{
            this.content = err.message;
          })

        break;
    }
  }

  edit() {
    this.router.navigateByUrl('/edit-content?content='+this.name+'&origin=remote'+'&topic='+this.topic);
  }
}
