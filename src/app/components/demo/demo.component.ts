import { Component, OnInit } from '@angular/core';
const YAML = require('yaml')

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {
  title = 'template-app';
  public mytime: Date = new Date();
  sss = YAML.stringify([true, false, 'maybe', null]);

  constructor() { }

  ngOnInit() {
  }

}
