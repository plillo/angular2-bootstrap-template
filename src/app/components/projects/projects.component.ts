import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AssetsService } from 'src/app/services/assets.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  costituzione: string = '';
  timeline: string = '';
  biblioteca: string = '';
  forms: string = '';

  constructor(private http: HttpClient, private _assetsService :AssetsService ) { }

  ngOnInit() {
        // get html pages from assets
        this._assetsService.setHtmlPropertiesFromAssets('costituzione,timeline,biblioteca,forms', this);
  }

  /*
  getHtmlFromAssets(propertyName: string) {
    this.http.get('./assets/html/'+propertyName+'.html', { responseType: "text" })
    .subscribe(
      (text) => {
        this[propertyName] = text;
      },
      () => {
        this[propertyName] = '#no page#';
      }
    );
  }
  */

}
