**Per servire la distribuzione in locale**
http-server dist/template-app

**Per riformattare**
SHIFT+ALT+F

**Per eseguire una build ottimizzata per la produzione**
(ng build --prod --aot --vendor-chunk --common-chunk --delete-output-path --buildOptimizer)
npm run ng build --prod --aot --vendor-chunk --common-chunk --delete-output-path --buildOptimizer

**backgrounds**
#eec894, #9db8d2, #e0e6a2, #99b98d, #7dc2d1, #c1c17b, #c5d1ef, #f6dbea, #d5e5db, #ced1dc, #e4c9a9, #a3ec95, #e09e81, #d0e8c7, #b4c17d, #c6f1cb, #90d08e, #96f0be, #d4928d, #a8e6e0
