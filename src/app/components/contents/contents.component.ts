import { Component, OnInit } from '@angular/core';
import { TabDirective } from 'ngx-bootstrap/tabs';

@Component({
  selector: 'app-contents',
  templateUrl: './contents.component.html',
  styleUrls: ['./contents.component.scss']
})
export class ContentsComponent implements OnInit {
  selectedTab: string = "Lista immagini";

  constructor() { }

  ngOnInit() {
  }

  onSelect(data)
  {
    this.selectedTab = data.heading;
  }

}
