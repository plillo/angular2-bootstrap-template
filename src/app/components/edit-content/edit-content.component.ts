import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { BackendService } from 'src/app/services/backend.service';
import { UserService } from 'src/app/services/user.service';

interface UploadResult {
  isImg: boolean
  name: string
  url: string
}

@Component({
  selector: 'app-edit-content',
  templateUrl: './edit-content.component.html',
  styleUrls: ['./edit-content.component.scss']
})
export class EditContentComponent implements OnInit {
  content: string;
  contentByURL: string;
  contentByMatrix: string;
  contentByAsset: string = '';

  contentOrigin: string;
  contentTopic: string;
  contentName: string;
  editMode:boolean = false;

  currentContent: Object;
  isOwner: boolean;

  options: any;

  constructor(private activatedRoute: ActivatedRoute, private http: HttpClient, private backendService: BackendService, private userService: UserService, private sanitizer: DomSanitizer) {
    this.doUpload = this.doUpload.bind(this);
  }

  doUpload(files: Array<File>): Promise<Array<UploadResult>> {
    // do upload file by yourself
    return Promise.resolve([{ name: 'xxx', url: 'xxx.png', isImg: true }]);
  }
   
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      let page = params['content'];
      let origin = params['origin'];
      let topic = params['topic'];

      // get origin
      // ==========
      if(origin) {
        this.contentOrigin = origin;
      }
      else
        this.contentOrigin = 'assets';

      // get topic
      // =========
      if(topic) {
        this.contentTopic = topic;
      }
      else
        this.contentTopic = '/contents/personal';  

      // get page
      // ========
      if(page) {
        this.contentName = page;

        switch(this.contentOrigin) {
          case 'assets': 
            this.http.get('./assets/html/'+page+'.html', { responseType: 'text'}).subscribe(
              data =>{
                this.contentByAsset = data;
              },
              err => {
                this.contentByAsset = err.message;
              }
            );
            break;
          case 'remote':
            let url = this.backendService.getAuthBackend() + '/core/1.0/users/' +this.userService.getUser().uuid + '/contents?type=text&topic='+this.contentTopic+'&name='+this.contentName;

            this.http.get(url, {})
            .subscribe(
              data =>{
                this.currentContent = data['content'];
                this.contentByAsset = this.currentContent['body']?this.currentContent['body']:'';

                this.isOwner = this.userService.getUser().uuid == this.currentContent['owner'];
              }, 
              err=>{
                this.contentByAsset = err.message;
              })
            break;
          default:
            this.contentByAsset = 'mismatched page origin';
        }
      }

      this.contentByURL = params['content'];
    });

    this.activatedRoute.params.subscribe(params => {
      let page = params['content'];

      if(page) 

        this.http.get('./assets/html/'+page+'.html', { responseType: 'text'}).subscribe(
          data =>{
            this.contentByAsset = data;
          },
          err => {
            this.contentByAsset = err;
          }
        );

      this.contentByMatrix = page;
    });
  }

  updateContent(){
    //let url = this.backendService.getAuthBackend() + '/core/1.0/users/' +this.userService.getUser().uuid + '/contents';
    //let formData = new FormData();
    //formData.append('uuid', this.currentContent['uuid']);
    //formData.append('body', this.contentByAsset);

    let url = this.backendService.getAuthBackend() + '/core/1.0/users/' +this.userService.getUser().uuid + '/contents/' + this.currentContent['uuid'];
    let formData = new FormData();
    formData.append('type', 'text');
    formData.append('body', this.contentByAsset);

    let headers = new HttpHeaders({'Content-Type': 'multipart/form-data'});
    this.http.put(url, formData/*, {'headers':headers}*/)
    .subscribe(
      data =>{

      }, 
      err=>{

      })
  }

  editContent() { 
    this.editMode = true;
  }

  showContent() {
    this.editMode = false;
  }

  isEditableContent() {
    return this.contentOrigin=='remote' && (this.isOwner || this.userService.isUserInOneOfRoles('root, admin'));
  }

  preRenderFunc(content: string) {
    return content.replace(/something/g, 'new value'); // must return a string
  }

  postRenderFunc(content: string) {
    return content.replace(/something/g, 'new value'); // must return a string
  }

  onEditorLoaded(event: any){
    //TODO
  }

  onPreviewDomChanged(event: any) {
    //TODO
  }
}

