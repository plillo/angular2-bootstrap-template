export function isEON(x: any): boolean {
  if (x == undefined || x == null || (isString(x) && x == "") || isNumber(x))
    return true;

  return false;
}

export function isNumber(x: any): x is number {
  return typeof x === "number";
}

export function isString(x: any): x is string {
  return typeof x === "string";
}

export function getFormattedDate(date: Date) {
  if (!date) return "";

  return (
    date.getDate() +
    "/" +
    (date.getMonth() + 1) +
    "/" +
    date.getFullYear() +
    " " +
    (date.getHours() < 10 ? "0" : "") +
    date.getHours() +
    ":" +
    (date.getMinutes() < 10 ? "0" : "") +
    date.getMinutes()
  );
}

export function getRandom(max: number, ciphers: number) {
  let s: string = 1 + Math.floor(Math.random() * max) + "";
  while (s.length < ciphers) s = "0" + s;
  return s;
}

export function formatNumberWithTrailingZeros(n: number, ciphers: number) {
  let s: string = n + "";
  while (s.length < ciphers) s = "0" + s;
  return s;
}
