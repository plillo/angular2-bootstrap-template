import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { BlogService } from '../../services/rest/blog.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
  selectedIndex: number = -1;
  blogUuid: string = undefined;
  searchString: string = '';
  filterChanged: Subject<string> = new Subject<string>();

  blogs: any[] = [];
  posts: any[] = [];
  
  constructor(private activatedRoute: ActivatedRoute, private blogService: BlogService, private router: Router) {
    this.filterChanged.pipe(debounceTime(1000)).subscribe((filter) => {
      this.loadList();
    });
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => { // use 'queryParamas' in place of 'params' if params are in queryString
      if(params['blogUuid']) {
        this.blogUuid = params['blogUuid'];
      }
    });

    this.loadBlogs();
    this.loadList();
  }

  loadList() {
    this.blogService.getAllPosts(this.searchString, (this.blogUuid && this.blogUuid!='') ? this.blogUuid:'').subscribe(
      (data: any) => {
        this.posts = data.posts;
      },
      (error) => {
        let e = error;
      }
    );
  }

  loadBlogs() {
    this.blogService.getAll().subscribe(
      (data: any) => {
        this.blogs = data.blogs;

        for(let k=0; k<this.blogs.length; k++) {
          if(this.blogs[k].uuid==this.blogUuid) {
            this.selectedIndex = k;
            break;
          }
        };

      },
      (error) => {
        let e = error;
      }
    );
  }

  blogChanged(selectedIndex: number): void {
    this.selectedIndex = selectedIndex;
    this.blogUuid = selectedIndex>=0?this.blogs[selectedIndex].uuid:undefined;
    this.loadList();
  }

  fireFilterChanged(filter: string) {
    this.filterChanged.next(filter);
  }

  click() {
  }

  edit(postUuid: string) {
    this.router.navigateByUrl("/blog/post;postUuid="+postUuid);
  }

  delete(postUuid: string) {
    this.blogService.deletePost(postUuid).subscribe(
      (data: any) => {
        let d = data;
      },
      (error) => {
        let e = error;
      }
    );
  }

}
