import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertModalContentComponent } from './alert-modal-content.component';

describe('AlertModalContentComponent', () => {
  let component: AlertModalContentComponent;
  let fixture: ComponentFixture<AlertModalContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertModalContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertModalContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
