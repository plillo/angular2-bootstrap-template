import { BlogModule } from './blog/blog.module';
import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SafePipe } from './pipes/safe';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// ngx-bootstrap imports
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsetConfig, TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule, TooltipConfig } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule, BsDropdownConfig } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule, BsDatepickerConfig, BsDaterangepickerConfig } from 'ngx-bootstrap/datepicker';
import { TimepickerModule, TimepickerConfig } from 'ngx-bootstrap/timepicker';
import { AccordionConfig, AccordionModule } from 'ngx-bootstrap/accordion';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { RouterModule } from '@angular/router';
import { LocalStorageModule } from 'angular-2-local-storage';
import { LMarkdownEditorModule } from 'ngx-markdown-editor';
import { MarkdownModule, MarkedOptions, MarkedRenderer } from 'ngx-markdown';

import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { CollapsedMenuComponent } from './components/shared/collapsed-menu/collapsed-menu.component';
import { NgxUiLoaderModule, NgxUiLoaderConfig, SPINNER, NgxUiLoaderService } from 'ngx-ui-loader';
import { DemoComponent } from './components/demo/demo.component';
import { ConfigurationProvider } from './classes/ConfigurationProvider';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CanActivateViaAuthGuard } from './classes/canactivate';
import { TokenInterceptor } from './services/token-interceptor.service';

import { FaConfig, FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faFilm, fas } from '@fortawesome/free-solid-svg-icons';
import { NotInProductionModalContent, PrivacyModalContent, SignupComponent, SignupConfirmationModalContent } from './components/signup/signup.component';
import { AlertModalContentComponent } from './components/alert-modal-content/alert-modal-content.component';
import { ProfileComponent } from './components/profile/profile.component';
import { DemosComponent } from './components/demos/demos.component';
import { DatesComponent } from './components/demos/dates/dates.component';
import { TimepickerComponent } from './components/demos/timepicker/timepicker.component';
import { DropdownComponent } from './components/demos/dropdown/dropdown.component';
import { ButtonsComponent } from './components/demos/buttons/buttons.component';
import { ConfirmationModalContent, SettingsComponent } from './components/settings/settings.component';
import { MultiFileUploadComponent } from './components/multi-file-upload/multi-file-upload.component';
import { FileUploadModule } from 'ng2-file-upload';
import { SingleFileUploadComponent } from './components/single-file-upload/single-file-upload.component';
import { AgmCoreModule } from '@agm/core';
import { ProjectsComponent } from './components/projects/projects.component';
import { ContactComponent } from './components/contact/contact.component';
import { TechDesignComponent } from './components/tech-design/tech-design.component';
import { WhoWeAreComponent } from './components/who-we-are/who-we-are.component';
import { AdministrationComponent } from './components/administration/administration.component';
import { BackendConfigComponent } from './components/backend-config/backend-config.component';
import { ContentComponent } from './components/content/content.component';
import { EditContentComponent } from './components/edit-content/edit-content.component';
import { ContentByUrlComponent } from './components/content-by-url/content-by-url.component';
import { ContentsComponent } from './components/contents/contents.component';
import { ContentsListComponent, EditImageContentModal, EditTextContentModal } from './components/contents-list/contents-list.component';
import { ImageContentUploadComponent } from './components/image-content-upload/image-content-upload.component';
import { TextContentEditorComponent } from './components/text-content-editor/text-content-editor.component';
import { ImageContentEditorComponent } from './components/image-content-editor/image-content-editor.component';

// configuration handling
// ======================
export function configurationProviderFactory(provider: ConfigurationProvider) {
  return () => provider.load();
}

// ngx-markdown renderer
// =====================
// function that returns `MarkedOptions` with renderer override
export function markedOptionsFactory(): MarkedOptions {
  const renderer = new MarkedRenderer();

  renderer.image = function(href, title, text) {    
    let size: string = '';    
    let clazz: string  = '';   
    let style: string  = '';                                                                                                                                                             
    if (title) {
      let splittedTitle: string[] = title.split(';');
      splittedTitle.forEach(function (item, index) {
        let token = item.trim();

        if(token.startsWith('class=')) {
          clazz = 'class="'+token.substring(6)+'"';
        }
        else if(token.startsWith('style=')) {
          style = 'style="'+token.substring(6)+'"';
        }
        else {
          let splitted: string[] = token.split('x');                                                                                          
          if (splitted[1]) {  
              if(splitted[0].trim().length>0 && parseInt(splitted[0])!=NaN) size += 'width=' + splitted[0].trim();
              if(splitted[1].trim().length>0 && parseInt(splitted[1])!=NaN) size += (size.length>0?' ':'')+'height=' + splitted[1].trim();
          } else {    
              if(splitted[0].trim().length>0 && parseInt(splitted[0])!=NaN) size += 'width=' + splitted[0].trim();                                                                                      
          }   
        } 
      });
    }

    let attributes = '';
    if(clazz.length>0)
      attributes += ' '+clazz;
    if(style.length>0)
      attributes += ' '+style;
    if(size.length>0)
      attributes += ' '+size;

    return ('<img src="' + href + '" alt="' + text + '"' + attributes + ' />');                                                        
  };

  return {
    renderer: renderer,
    gfm: true,
    breaks: false,
    pedantic: false,
    smartLists: true,
    smartypants: false,
  };
}




const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  bgsType: SPINNER.ballScaleMultiple,
  hasProgressBar: false
};

@NgModule({
  declarations: [
    // pipes
    SafePipe,

    // components
    AppComponent,
    LandingPageComponent,
    NavbarComponent,
    FooterComponent,
    CollapsedMenuComponent,
    DemoComponent,
    SignupComponent,
    AlertModalContentComponent,
    PrivacyModalContent,
    NotInProductionModalContent,
    ConfirmationModalContent,
    SignupConfirmationModalContent,
    EditTextContentModal,
    EditImageContentModal,
    ProfileComponent,
    DemosComponent,
    DatesComponent,
    TimepickerComponent,
    DropdownComponent,
    ButtonsComponent,
    SettingsComponent,
    MultiFileUploadComponent,
    SingleFileUploadComponent,
    ProjectsComponent,
    ContactComponent,
    TechDesignComponent,
    WhoWeAreComponent,
    AdministrationComponent,
    BackendConfigComponent,
    ContentComponent,
    EditContentComponent,
    ContentByUrlComponent,
    ContentsComponent,
    ContentsListComponent,
    ImageContentUploadComponent,
    TextContentEditorComponent,
    ImageContentEditorComponent
  ],
  imports: [
    BlogModule,
    BrowserModule,
    NgxUiLoaderModule.forRoot({
      bgsType: SPINNER.ballScaleMultiple,
      hasProgressBar: false
    }),
    AppRoutingModule,
    FormsModule,
    LMarkdownEditorModule,
    RouterModule,
    JwBootstrapSwitchNg2Module,
    LocalStorageModule.forRoot({
      storageType: 'localStorage'
    }),
    FontAwesomeModule,
    HttpClientModule,
    BrowserAnimationsModule,
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AccordionModule.forRoot(),
    TimepickerModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    FileUploadModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAcr5GOERGEt8kw6AKGWZOmR8weth6y-OQ', // https://developers.google.com/maps/documentation/javascript/get-api-key
      libraries: ['places']
    }),
    MarkdownModule.forRoot({ 
      loader: HttpClient,
      markedOptions: {
        provide: MarkedOptions,
        useFactory: markedOptionsFactory,
      }
    })
  ],
  providers: [
    ConfigurationProvider,
    CanActivateViaAuthGuard,
    { provide: APP_INITIALIZER, useFactory: configurationProviderFactory, deps: [ConfigurationProvider], multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    FaConfig,
    FaIconLibrary,
    
    // ngx-bootstrap configs
    TooltipConfig,
    TabsetConfig,
    BsDropdownConfig, 
    BsDatepickerConfig, 
    BsDaterangepickerConfig, 
    TimepickerConfig,
    AccordionConfig,
    NgxUiLoaderService
  ],
  entryComponents: [
    AlertModalContentComponent,
    PrivacyModalContent,
    NotInProductionModalContent,
    ConfirmationModalContent,
    SignupConfirmationModalContent,
    EditTextContentModal,
    EditImageContentModal
],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
    library.addIcons(faFilm);
  }
}
