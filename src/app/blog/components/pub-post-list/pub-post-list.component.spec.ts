import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PubPostListComponent } from './pub-post-list.component';

describe('PubPostListComponent', () => {
  let component: PubPostListComponent;
  let fixture: ComponentFixture<PubPostListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubPostListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubPostListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
