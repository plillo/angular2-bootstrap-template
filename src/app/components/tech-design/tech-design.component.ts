import { Component, OnDestroy, OnInit } from '@angular/core';
import { CollapsedMenuService } from 'src/app/services/collapsed-menu.service';

@Component({
  selector: 'app-tech-design',
  templateUrl: './tech-design.component.html',
  styleUrls: ['./tech-design.component.scss']
})
export class TechDesignComponent implements OnInit, OnDestroy {
   // Level '2' collapsed menu items
   public items: any[] = [
    // =========
    // Componenti
    {
      level: 2,
      label: 'tecnologie',
      group: 'tecnologie'
    },
    {
      level: 2,
      sublevel: 1,
      groupMaster: 'tecnologie',
      routerLink: ['/tech/osgi'],
      label: "OSGi",
    },
    {
      level: 2,
      sublevel: 1,
      groupMaster: 'tecnologie',
      routerLink: ['/tech/jaxrs'],
      label: "JAX-RS",
    },
    {
      level: 2,
      sublevel: 1,
      groupMaster: 'tecnologie',
      routerLink: ['/teck/angular2'],
      label: "Angular2",
    },

    // ===========
    // Application
    {
      level: 2,
      label: 'Infrastruttura',
      group: 'infrastruttura'
    },
    {
      level: 2,
      sublevel: 1,
      groupMaster: 'infrastruttura',
      routerLink: ['/demo/demo'],
      label: "Cloud: Amazon AWS",
    },
  ];  

  constructor(private collapsedMenuService: CollapsedMenuService) { }

  ngOnInit() {
    // ADD items to the collapsed menu service
    this.collapsedMenuService.addItems(this.items, 2, 'Componenti');
  }

  ngOnDestroy(): void {
      // ADD items to the collapsed menu service
      this.collapsedMenuService.removeItems(2);
  }

  sidebarToggle() {
    this.collapsedMenuService.sidebarToggle();
  }

}
