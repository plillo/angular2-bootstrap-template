import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../services/rest/blog.service';

@Component({
  selector: 'app-pub-post-list',
  templateUrl: './pub-post-list.component.html',
  styleUrls: ['./pub-post-list.component.scss']
})
export class PubPostListComponent implements OnInit {

  posts: any[] = [];
  
  constructor(private blogService: BlogService) { }

  ngOnInit() {
    // ADD items to the collapsed menu service
    this.blogService.getAllPubPosts('').subscribe(
      (data: any) => {
        this.posts = data.posts;
      },
      (error) => {
        let e = error;
      }
    );
  }

  getPictureUrl(post: any) {
    return post.owner.picture?'https://downloads.salentopoli.it/users/'+post.owner.picture:'./assets/images/no-user-picture.png';
  }

  getDate(date: string) {
    return new Date(date).toLocaleDateString("it-IT");
  }

  click() {
  }

}
