import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PubBlogListComponent } from './pub-blog-list.component';

describe('PubBlogListComponent', () => {
  let component: PubBlogListComponent;
  let fixture: ComponentFixture<PubBlogListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubBlogListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubBlogListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
