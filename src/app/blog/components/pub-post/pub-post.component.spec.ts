import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PubPostComponent } from './pub-post.component';

describe('PubPostComponent', () => {
  let component: PubPostComponent;
  let fixture: ComponentFixture<PubPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PubPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PubPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
