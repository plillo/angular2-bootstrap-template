import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { CollapsedMenuService } from 'src/app/services/collapsed-menu.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-blog-area',
  templateUrl: './blog-area.component.html',
  styleUrls: ['./blog-area.component.scss']
})
export class BlogAreaComponent implements OnInit {
  // Level '2' collapsed menu items
  public items: any[] = [];  


  constructor(
    private userService: UserService, 
    private collapsedMenuService: CollapsedMenuService, 
    private router: Router) { 
  }

  ngOnInit() {
    // ADD items to the collapsed menu service
    this.collapsedMenuService.addItems(this.items, 2, 'Componenti');
  }

  ngOnDestroy(): void {
      // ADD items to the collapsed menu service
      this.collapsedMenuService.removeItems(2);
  }

  sidebarToggle() {
    this.collapsedMenuService.sidebarToggle();
  }

  newPost() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
  }

  goto(link):any {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.router.navigate([link]);
  }

  isUserAdmin(): boolean {
    return this.userService.isUserInOneOfRoles("admin, root");
  }
}
