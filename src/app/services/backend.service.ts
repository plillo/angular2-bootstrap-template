import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  authProductionUrl = "https://api.salentopoli.it/services"; //api.salentopoli.it
  blogProductionUrl = "https://api.salentopoli.it/services"; //api.salentopoli.it
  applicationProductionUrl = "https://api.salentopoli.it/services"; //api.salentopoli.it

  constructor(private localStorage: LocalStorageService) { }

  getProductionAuthBackend():string {
    return this.authProductionUrl;
  };

  getProductionBlogBackend():string {
    return this.blogProductionUrl;
  };

  getProductionApplicationBackend():string {
    return this.applicationProductionUrl;
  };

  // ===

  getAuthBackend():string {
    status = this.localStorage.get("developmentAuthBackendStatus");
    if("true"===status) return this.localStorage.get("developmentAuthBackend");
      
    return this.authProductionUrl;
  };

  setAuthBackend(url:string) {
      if(url == ''){
          this.authProductionUrl = "http://localhost:8080/api"
      }
      else{
        this.authProductionUrl = url;
      }
  }

  // ===

  getBlogBackend():string {
    status = this.localStorage.get("developmentBlogBackendStatus");
    if("true"===status) return this.localStorage.get("developmentBlogBackend");
      
    return this.blogProductionUrl;
  };

  setBlogBackend(url:string) {
      if(url == ''){
          this.blogProductionUrl = "http://localhost:8080/api"
      }
      else{
        this.blogProductionUrl = url;
      }
  }

  // ===

  getApplicationBackend():string {
    status = this.localStorage.get("developmentApplicationBackendStatus");
    if("true"===status) return this.localStorage.get("developmentApplicationBackend");
    
    return this.applicationProductionUrl;
  };

  setApplicationBackend(url:string) {
    if(url == ''){
        this.applicationProductionUrl = "http://localhost:8080/api"
    }
    else{
      this.applicationProductionUrl = url;
    }
  }

  // ===

  isMocking(): boolean {
    return this.localStorage.get("mockingMode");
  }
}

