import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AssetsService {

  constructor(private http: HttpClient) { }

  setHtmlPropertyFromAssets(propertyName: string, obj: object) {
    this.http.get('./assets/html/'+propertyName+'.html', { responseType: "text" })
    .subscribe(
      (text) => {
        obj[propertyName] = text;
      },
      () => {
        obj[propertyName] = '#no page#';
      }
    );
  }

  setHtmlPropertiesFromAssets(propertyNames: string, obj: object) {
    if(propertyNames)
      for (let name of propertyNames.split(',')) {
        this.setHtmlPropertyFromAssets(name.trim(), obj);
      }
  }
}
