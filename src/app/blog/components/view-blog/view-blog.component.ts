import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { BlogService } from '../../services/rest/blog.service';

@Component({
  selector: 'app-view-blog',
  templateUrl: './view-blog.component.html',
  styleUrls: ['./view-blog.component.scss']
})
export class ViewBlogComponent implements OnInit {
  @Input() uuid: string = undefined;
  searchString: string;

  public blog = {
    uuid: undefined,
    contentUuid: undefined,
    owner: undefined,
    body: "",
    posts: [],
    category: "",
    description: "",
    title: "",
    banner: "",
    URL: undefined
  };
  content: string = '';
  otherBlogs: any;
  actualPostUuid: string = undefined;
  date: string = undefined;

  constructor(private activatedRoute: ActivatedRoute, private blogService: BlogService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => { // use 'queryParamas' in place of 'params' if params are in queryString
    if(params['post-uuid']) {
      this.actualPostUuid = params['post-uuid'];
    };
    if(params['uuid']) {
      this.uuid = params['uuid'];
      this.loadBlog(this.uuid, this.actualPostUuid);
    };
  });
  }

  loadPost(actualPostUuid?: string) {
    if(actualPostUuid) {
      this.actualPostUuid = actualPostUuid;
    }
    this.ngxService.start();
    this.blogService.getPost(actualPostUuid).subscribe(
      (data: any) => {
        this.ngxService.stop();
        this.actualPostUuid = actualPostUuid;
        this.content = data.post.body;
        this.date = data.post.date;
      },
      (error) => {
        this.ngxService.stop();
        let e = error;
      }
    );
  }

  loadBlog(uuid: string, postUuid: string) {
    this.uuid = uuid;
    this.actualPostUuid = postUuid;

    this.blogService.get(this.uuid).subscribe(
      (data: any) => {
        this.blog.uuid = data.blog.uuid;
        this.blog.contentUuid = data.blog.contentUuid;
        this.blog.owner = data.blog.owner;
        this.blog.body = data.blog.body;
        this.blog.title = data.blog.title;
        this.blog.description = data.blog.description;
        this.blog.category = data.blog.category;
        this.blog.banner = data.blog.banner;
        this.blog.posts = data.blog.posts;

        if(this.actualPostUuid)
          this.loadPost(this.actualPostUuid);
        else if(this.blog.posts.length>0)
          this.loadPost(this.blog.posts[0].uuid);
        else
          this.content = '';

        this.loadAuthorBlogs(this.blog.owner.uuid);
      },
      (error) => {
        let e = error;
      }
    );
  }

  loadAuthorBlogs(userUuid: string) {
    this.ngxService.start();
    this.blogService.getAllByUser(userUuid).subscribe(
      (data: any) => {
        this.ngxService.stop();
        this.otherBlogs = data.blogs;
      },
      (error) => {
        this.ngxService.stop();
        let e = error;
      }
    );
  }

  getPictureUrl() {
    return this.blog.owner.picture?'https://downloads.salentopoli.it/users/'+this.blog.owner.picture:'./assets/images/no-user-picture.png';
  }

  getDate(date: string) {
    return date?new Date(date).toLocaleDateString("it-IT"):'';
  }

}
