import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TimepickerComponent } from 'ngx-bootstrap/timepicker';
import { BlogAreaComponent } from './blog/components/blog-area/blog-area.component';
import { BlogListComponent } from './blog/components/blog-list/blog-list.component';
import { EditBlogComponent } from './blog/components/edit-blog/edit-blog.component';
import { EditPostComponent } from './blog/components/edit-post/edit-post.component';
import { PostListComponent } from './blog/components/post-list/post-list.component';
import { ViewBlogComponent } from './blog/components/view-blog/view-blog.component';
import { CanActivateViaAuthGuard } from './classes/canactivate';
import { AdministrationComponent } from './components/administration/administration.component';
import { BackendConfigComponent } from './components/backend-config/backend-config.component';
import { ContentByUrlComponent } from './components/content-by-url/content-by-url.component';
import { ContentComponent } from './components/content/content.component';
import { DemoComponent } from './components/demo/demo.component';
import { ButtonsComponent } from './components/demos/buttons/buttons.component';
import { DatesComponent } from './components/demos/dates/dates.component';
import { DemosComponent } from './components/demos/demos.component';
import { DropdownComponent } from './components/demos/dropdown/dropdown.component';
import { EditContentComponent } from './components/edit-content/edit-content.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { SettingsComponent } from './components/settings/settings.component';
import { SignupComponent } from './components/signup/signup.component';
import { TechDesignComponent } from './components/tech-design/tech-design.component';
import { WhoWeAreComponent } from './components/who-we-are/who-we-are.component';


const routes: Routes = [
  { path: '', redirectTo: 'landing', pathMatch: 'full' },
  { path: 'home', component: LandingPageComponent },
  { path: 'landing', component: LandingPageComponent },
  { path: 'blog-view', component: ViewBlogComponent },
  { path: 'administration', component: AdministrationComponent, canActivate:  [CanActivateViaAuthGuard],
    children: [
      { path: 'backend', component: BackendConfigComponent }
    ]
  },
  { path: 'projects', component: ProjectsComponent },
  { path: 'tech', component: TechDesignComponent },
  { path: 'whoweare', component: WhoWeAreComponent },
  { path: 'blog', component: BlogAreaComponent,
    children: [
      { path: 'blog', component: EditBlogComponent },
      { path: 'blog-list', component: BlogListComponent },
      { path: 'post', component: EditPostComponent, runGuardsAndResolvers: 'always' },
      { path: 'post-list', component: PostListComponent },
    ] 
  },
  { path: 'demo', component: DemosComponent,
    children: [
      { path: 'dates', component: DatesComponent },
      { path: 'timepicker', component: TimepickerComponent },
      { path: 'dropdown', component: DropdownComponent },
      { path: 'demo', component: DemoComponent },
      { path: 'buttons', component: ButtonsComponent },
    ] 
  },
  { path: 'signup', component: SignupComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'edit-content', component: EditContentComponent },
  { path: 'content-by-url', component: ContentByUrlComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    onSameUrlNavigation: 'reload',
    useHash: true
  })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
