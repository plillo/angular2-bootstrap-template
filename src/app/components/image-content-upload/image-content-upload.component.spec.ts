import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageContentUploadComponent } from './image-content-upload.component';

describe('ImageContentUploadComponent', () => {
  let component: ImageContentUploadComponent;
  let fixture: ComponentFixture<ImageContentUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageContentUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageContentUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
