import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-content-by-url',
  templateUrl: './content-by-url.component.html',
  styleUrls: ['./content-by-url.component.scss']
})
export class ContentByUrlComponent implements OnInit {
  origin: string;
  owner: string;
  topic: string;
  name: string;
  continueLink: boolean;

  constructor(private activatedRoute: ActivatedRoute) {
  }
   
  ngOnInit() {
    // Get parameters from URL
    this.activatedRoute.queryParams.subscribe(params => {
      let origin = params['origin'];
      let owner = params['owner'];
      let topic = params['topic'];
      let name = params['name'];
      let continueLink: boolean = params['continue']?params['continue']:false;

      // get origin
      // ==========
      if(origin) {
        this.origin = origin;
      }
      else
        this.origin = 'assets'; //default

      // get topic
      // =========
      if(topic) {
        this.topic = topic;
      }
      else
        this.topic = '/contents/personal'; //default  

      // get name
      // ========
      if(name) {
        this.name = name;
      }

      // get owner
      // =========
      if(owner) {
        this.owner = owner;
      }

      // =========
      if(continueLink) {
        this.continueLink = continueLink;
      }
    });
  }
}
