import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentByUrlComponent } from './content-by-url.component';

describe('ContentByUrlComponent', () => {
  let component: ContentByUrlComponent;
  let fixture: ComponentFixture<ContentByUrlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentByUrlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentByUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
