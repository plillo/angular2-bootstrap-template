import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { BlogService } from '../../services/rest/blog.service';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss']
})
export class BlogListComponent implements OnInit {

  blogs: any[] = [];
  
  constructor(private blogService: BlogService, private router: Router, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
    this.getBlogs();
  }

  getBlogs() {
    this.ngxService.start();
    this.blogService.getAll().subscribe(
      (data: any) => {
        this.ngxService.stop();
        this.blogs = data.blogs;
      },
      (error) => {
        this.ngxService.stop();
        let e = error;
      }
    );
  }

  click() {
  }

  edit(blogUuid: string) {
    this.router.navigateByUrl("/blog/blog;blogUuid="+blogUuid);
  }

  listPosts(blogUuid: string) {
    this.router.navigateByUrl("/blog/post-list;blogUuid="+blogUuid);
  }
  delete(blogUuid: string) {
    this.blogService.delete(blogUuid).subscribe(
      (data: any) => {
        let d = data;
        this.getBlogs();
      },
      (error) => {
        let e = error;
      }
    );
  }

}
