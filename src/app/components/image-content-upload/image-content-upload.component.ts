import { Component, OnInit, Input } from '@angular/core';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { ApplicationService } from 'src/app/services/application.service';
import { BackendService } from 'src/app/services/backend.service';
import { UserService } from 'src/app/services/user.service';

export interface UploadItem {
  type: string,
  name: string;
  lang: string;
  description: string,
  tags: string,
  topic: string
}

@Component({
  selector: 'app-image-content-upload',
  templateUrl: './image-content-upload.component.html',
  styleUrls: ['./image-content-upload.component.scss']
})
export class ImageContentUploadComponent implements OnInit {
  @Input() url: string = ''; 
  @Input() topic: string = '';

  public fields: any =  {name:undefined, lang:undefined, description:undefined, tags:undefined};

  // File uploader
  // =============
  public uploader: FileUploader;
  
  // Detail items
  public items: UploadItem[] = [];

  actualVisibleRowIndex:number = -1;
  hasBaseDropZoneOver:boolean;
  hasAnotherDropZoneOver:boolean;
  response:string;

  constructor (private backendService: BackendService, private userService: UserService, private applicationService: ApplicationService){
  }

  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e:any):void {
    this.hasAnotherDropZoneOver = e;
  }

  public uploadingURL() {
    return this.backendService.getAuthBackend() + this.url.replace('UUID', this.userService.getUser().uuid);
  }

  public isVisible(index) {
    return index==this.actualVisibleRowIndex;
  }

  public setVisible(index) {
    if(this.actualVisibleRowIndex == index)
      this.actualVisibleRowIndex = -1;
    else 
      this.actualVisibleRowIndex = index;
  }

  public remove(index) {
    this.uploader.queue[index].remove();
    this.items.splice(index, 1);
  }

  ngOnInit() {
    this.uploader = new FileUploader({
      url: this.uploadingURL(),
      authToken: `Bearer ${this.userService.getToken()}`,
      disableMultipart: false, // 'DisableMultipart' must be 'true' for formatDataFunction to be called.
      method: 'post',
      itemAlias: 'picture',
      allowedFileType: ['image'],
      headers: [
        {name: 'Tenant-ID', value: this.applicationService.getTenantID()},
        {name: 'Accept-Language', value: this.userService.getLocale()}
      ]
    });

    this.hasBaseDropZoneOver = false;
    this.hasAnotherDropZoneOver = false;

    this.response = '';

    this.uploader.response.subscribe(res => {
      let resObj = JSON.parse(res);

      let name = res.content.originalName;
      for(var k = 0; k < this.items.length; k++){
        if(this.items[k].name==name) {
          this.remove(k);
          break;
        }
      }
      //this.userService.relogin();
    });

    this.uploader.onAfterAddingFile = (fileItem: FileItem): any => {
      this.items.push({type:'image', name:fileItem.file.name, lang:'',description:'', tags: '', topic: ''});
    };

    this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
      for(var k = 0; k < this.uploader.queue.length; k++){
        if(this.uploader.queue[k]==fileItem) {
          if(!this.isEON(this.items[k].type)) 
          form.append('type', this.items[k].type);

          if(!this.isEON(this.items[k].name)) 
          form.append('name', this.items[k].name);

          if(!this.isEON(this.items[k].lang)) 
          form.append('lang', this.items[k].lang);

          if(!this.isEON(this.items[k].tags)) 
            form.append('tags', this.items[k].tags);
          
          if(!this.isEON(this.topic)) {
            if(!this.isEON(this.items[k].topic)) 
              form.append('topic', this.topic + (this.items[k].topic.startsWith('/') ? this.items[k].topic : '/'+this.items[k].topic));
            else
              form.append('topic', this.topic);
          }

          if(!this.isEON(this.items[k].description)) 
            form.append('description', this.items[k].description);
        }
      }  
    };
  }

  private isEON(s:string):boolean {
    return (s==undefined || s=='' || s==null);
  }
}