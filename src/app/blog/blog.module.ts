import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EditPostComponent } from './components/edit-post/edit-post.component';
import { ViewBlogComponent } from './components/view-blog/view-blog.component';
import { BlogAreaComponent } from './components/blog-area/blog-area.component';
import { RouterModule } from '@angular/router';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { FormsModule } from '@angular/forms';
import { MarkdownModule } from 'ngx-markdown';
import { LMarkdownEditorModule } from 'ngx-markdown-editor';
import { PostListComponent } from './components/post-list/post-list.component';
import { BlogListComponent } from './components/blog-list/blog-list.component';
import { EditBlogComponent } from './components/edit-blog/edit-blog.component';
import { PubPostListComponent } from './components/pub-post-list/pub-post-list.component';
import { PubBlogListComponent } from './components/pub-blog-list/pub-blog-list.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { PubPostComponent } from './components/pub-post/pub-post.component';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        BsDropdownModule,
        BsDatepickerModule,
        MarkdownModule,
        LMarkdownEditorModule,
        JwBootstrapSwitchNg2Module,
    ],
    declarations: [
        EditPostComponent,
        ViewBlogComponent,
        BlogAreaComponent,
        PostListComponent,
        BlogListComponent,
        EditBlogComponent,
        PubPostListComponent,
        PubBlogListComponent,
        PubPostComponent
    ],
    providers: [
    ],
    entryComponents: [
    ],
    exports:[EditBlogComponent, EditPostComponent, ViewBlogComponent, PubPostComponent, PubPostListComponent]
})
export class BlogModule { }