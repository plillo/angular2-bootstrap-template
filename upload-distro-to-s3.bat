aws s3 sync ./dist/template-app s3://salentopoli.awsfarm.hash.it --delete
aws s3 sync ./src s3://backups.awsfarm.hash.it/salentopoli/webfrontend/src --delete
aws configure set preview.cloudfront true
aws cloudfront create-invalidation --distribution-id E3SN6SYBJ6RWUG --paths "/*"