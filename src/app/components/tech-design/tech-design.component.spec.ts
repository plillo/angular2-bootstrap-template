import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechDesignComponent } from './tech-design.component';

describe('TechDesignComponent', () => {
  let component: TechDesignComponent;
  let fixture: ComponentFixture<TechDesignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechDesignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
