import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BackendService } from 'src/app/services/backend.service';
import { UserService } from 'src/app/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  constructor(private http: HttpClient, private userService: UserService, private backendService: BackendService) { }

  getAll() {
    return this.http.get(this.backendService.getBlogBackend() + '/blog-1.0/users/' +this.userService.getUser().uuid + '/blogs', {});
  }

  getAllByUser(userUuid: string) {
    return this.http.get(this.backendService.getBlogBackend() + '/blog-1.0/users/' +userUuid + '/blogs', {});
  }

  get(uuid: string) {
    return this.http.get(this.backendService.getBlogBackend() + '/blog-1.0/users/' +this.userService.getUser().uuid + '/blogs/'+ uuid, {});
  }

  post(data: any) {
    return this.http.post(this.backendService.getBlogBackend() + '/blog-1.0/users/' +this.userService.getUser().uuid + '/blogs', data);
  }

  put(uuid: string, data: any) {
    return this.http.put(this.backendService.getBlogBackend() + '/blog-1.0/users/' +this.userService.getUser().uuid + '/blogs/'+uuid, data);
  }

  delete(uuid: string) {
    return this.http.delete(this.backendService.getBlogBackend() + '/blog-1.0/users/' +this.userService.getUser().uuid + '/blogs/'+uuid);
  }

  // === POST
  getAllPosts(filterString: string, uuidBlog: string) {
    const params = {};
    if(filterString && filterString!='')
      params["filter"] = filterString;
    if(uuidBlog && uuidBlog!='')
      params["uuidBlog"] = uuidBlog;

    return this.http.get(this.backendService.getBlogBackend() + '/blog-1.0/users/' +this.userService.getUser().uuid + '/posts', {params:params});
  }

  getAllPostsByBlog(blogUuid: string) {
    return this.http.get(this.backendService.getBlogBackend() + '/blog-1.0/users/' +this.userService.getUser().uuid + '/blogs/'+ blogUuid, {});
  }

  getPost(postUuid: string) {
    return this.http.get(this.backendService.getBlogBackend() + '/blog-1.0/blogs/posts/'+ postUuid, {});
  }

  postPost(blogUuid: string, data: any) {
    return this.http.post(this.backendService.getBlogBackend() + '/blog-1.0/users/' +this.userService.getUser().uuid + '/blogs/'+blogUuid+'/posts', data);
  }

  putPost(postUuid: string, data: any) {
    return this.http.put(this.backendService.getBlogBackend() + '/blog-1.0/users/' +this.userService.getUser().uuid + '/posts/'+postUuid, data);
  }

  deletePost(postUuid: string) {
    return this.http.delete(this.backendService.getBlogBackend() + '/blog-1.0/users/' +this.userService.getUser().uuid + '/posts/'+postUuid);
  }

  getAllPubPosts(orderby: string) {
    const filter = {};
    if(orderby && orderby!='')
      filter["orderby"] = orderby;

    return this.http.get(this.backendService.getBlogBackend() + '/blog-1.0/blogs/posts', {params: filter});
  }
}
