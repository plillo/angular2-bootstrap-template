import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { UploadResult } from 'ngx-markdown-editor';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Subscription } from 'rxjs';
import { BlogService } from '../../services/rest/blog.service';

@Component({
  selector: 'app-edit-blog',
  templateUrl: './edit-blog.component.html',
  styleUrls: ['./edit-blog.component.scss']
})
export class EditBlogComponent implements OnInit {
  @Input() uuid: string = undefined;
  
  public blog = {
    uuid: undefined,
    contentUuid: undefined,
    body: "",
    category: "",
    description: "",
    title: "",
    banner: "",
    publicated: false,
    URL: undefined
  };

  // Per gestire la re-inizializzazione quando si ricarica la rotta
  navigationSubscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private blogService: BlogService, private router: Router, private ngxService: NgxUiLoaderService) {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.initialise();
      }
    });
  }

  initialise() {
    this.uuid = undefined;

    this.blog = {
      uuid: undefined,
      contentUuid: undefined,
      body: "",
      category: "",
      description: "",
      title: "",
      banner: "",
      publicated: false,
      URL: undefined
    };
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => { // use 'queryParamas' in place of 'params' if params are in queryString
      if(params['blogUuid']) {
        this.uuid = params['blogUuid'];

        this.blogService.get(this.uuid).subscribe(
          (data: any) => {
            this.blog.uuid = data.blog.uuid;
            this.blog.contentUuid = data.blog.contentUuid;
            this.blog.body = data.blog.body;
            this.blog.title = data.blog.title;
            this.blog.description = data.blog.description;
            this.blog.category = data.blog.category;
            this.blog.publicated = data.blog.publicated?data.blog.publicated:false;
            this.blog.banner = data.blog.banner;
          },
          (error) => {
            let e = error;
          }
        );
      }
    });
  }

  create(): void {
    this.ngxService.start();
    this.blogService.post(this.blog).subscribe(
      (data) => {
        this.ngxService.stop();
        let d = data;
      },
      (error) => {
        this.ngxService.stop();
        let e = error;
      }
    );
  }  

  update(): void {
    this.ngxService.start();
    this.blogService.put(this.uuid, this.blog).subscribe(
      (data) => {
        this.ngxService.stop();
        let d = data;
      },
      (error) => {
        this.ngxService.stop();
        let e = error;
      }
    );
  }

  togglePublicated() {
      //this.blog.publicated = !this.blog.publicated;
  }

    // MARKDOWN MANAGEMENT
  // ===================
  doUpload(files: Array<File>): Promise<Array<UploadResult>> {
    // do upload file by yourself
    return Promise.resolve([{ name: "xxx", url: "xxx.png", isImg: true }]);
  }

  preRenderFunc(content: string) {
    return content.replace(/something/g, "new value"); // must return a string
  }

  postRenderFunc(content: string) {
    return content.replace(/something/g, "new value"); // must return a string
  }

  onEditorLoaded(event: any) {
    //TODO
  }

  onPreviewDomChanged(event: any) {
    //TODO
  }

}
