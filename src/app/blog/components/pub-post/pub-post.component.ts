import { Component, Input, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { BlogService } from '../../services/rest/blog.service';

@Component({
  selector: 'app-pub-post',
  templateUrl: './pub-post.component.html',
  styleUrls: ['./pub-post.component.scss']
})
export class PubPostComponent implements OnInit {
  @Input() uuid: string = undefined;
  content: string = '';
  blogUuid: any;
  blogTitle: any;
  post: any;

  constructor(private blogService: BlogService, private ngxService: NgxUiLoaderService) {
  }

  ngOnInit() {
    this.ngxService.start();
    this.blogService.getPost(this.uuid).subscribe(
      (data: any) => {
        this.ngxService.stop();

        this.post = data.post;

        this.content = data.post.body;
        this.blogUuid = data.post.blogUuid;
        this.blogTitle = data.post.blogTitle;
      },
      (error) => {
        this.ngxService.stop();
        let e = error;
      }
    );
  }

  getPictureUrl() {
    if(!this.post)
      return undefined;
    return this.post.owner.picture?'https://downloads.salentopoli.it/users/'+this.post.owner.picture:'./assets/images/no-user-picture.png';
  }

  getDate() {
    if(!this.post) return '';
    return new Date(this.post.date).toLocaleDateString("it-IT");
  }

}
