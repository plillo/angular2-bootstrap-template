import { keyframes } from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { UploadResult } from 'ngx-markdown-editor';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Observable, Subscription } from 'rxjs';
import { BlogService } from '../../services/rest/blog.service';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit {
  @Input() uuid: string = undefined;
  
  public post = {
    uuid: undefined,
    blogUuid: undefined,
    contentUuid: undefined,
    body: "",
    name: "",
    description: "",
    date: undefined,
    lang: "IT_it",
    tags: "",
    title: "",
    banner: "",
    URL: undefined
  };

  options: any;

  blogs: any[] = [];
  selectedIndex: number = -1;

  // Per gestire la re-inizializzazione quando si ricarica la rotta
  navigationSubscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private blogService: BlogService, private router: Router, private ngxService: NgxUiLoaderService) { 
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.initialise();
      }
    });
  }

  initialise() {
    this.uuid = undefined;
    this.selectedIndex = -1;

    this.post = {
      uuid: undefined,
      blogUuid: undefined,
      contentUuid: undefined,
      body: "",
      name: "",
      description: "",
      date: undefined,
      lang: "IT_it",
      tags: "",
      title: "",
      banner: "",
      URL: undefined
    };
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => { // use 'queryParamas' in place of 'params' if params are in queryString
      if(params['postUuid']) {
        this.uuid = params['postUuid'];

        this.ngxService.start();
        this.blogService.getPost(this.uuid).subscribe(
          (data: any) => {
            this.ngxService.stop();

            this.post.uuid = data.post.uuid;
            this.post.contentUuid = data.post.contentUuid;
            this.post.body = data.post.body;
            this.post.blogUuid = data.post.blogUuid;
            this.post.title = data.post.title;
            this.post.name = data.post.name;
            this.post.description = data.post.description;
            this.post.banner = data.post.banner;
            this.post.tags = data.post.tags;
            this.post.date = new Date(data.post.date);

            this.loadBlogs();
          },
          (error) => {
            this.ngxService.stop();
            let e = error;
          }
        );
      }
      else
        this.loadBlogs();
    });
  }

  loadBlogs() {
    this.blogService.getAll().subscribe(
      (data: any) => {
        this.blogs = data.blogs;

        if(this.uuid) {
          for(let k=0; k<this.blogs.length; k++) {
            if(this.blogs[k].uuid==this.post.blogUuid) {
              this.selectedIndex = k;
              break;
            }
          };
        }
        else if(this.blogs.length==1)
          this.selectedIndex = 0;
      },
      (error) => {
        let e = error;
      }
    );
  }

  blogChanged(selectedIndex: number): void {
    this.selectedIndex = selectedIndex;
    this.post.blogUuid = this.blogs[selectedIndex].uuid;
  }

  create(): void {
    this.ngxService.start();
    this.blogService.postPost(this.blogs[this.selectedIndex].uuid, this.post).subscribe(
      (data) => {
        let d = data;
        this.ngxService.stop();
      },
      (error) => {
        let e = error;
        this.ngxService.stop();
      }
    );
  }  

  update(): void {
    this.ngxService.start();
    this.blogService.putPost(this.uuid, this.post).subscribe(
      (data) => {
        let d = data;
        this.ngxService.stop();
      },
      (error) => {
        let e = error;
        this.ngxService.stop();
      }
    );
  } 

  // MARKDOWN MANAGEMENT
  // ===================
  doUpload(files: Array<File>): Promise<Array<UploadResult>> {
    // do upload file by yourself
    return Promise.resolve([{ name: "xxx", url: "xxx.png", isImg: true }]);
  }

  preRenderFunc(content: string) {
    return content.replace(/something/g, "new value"); // must return a string
  }

  postRenderFunc(content: string) {
    return content.replace(/something/g, "new value"); // must return a string
  }

  onEditorLoaded(event: any) {
    //TODO
  }

  onPreviewDomChanged(event: any) {
    //TODO
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {  
       this.navigationSubscription.unsubscribe();
    }
  }
}
